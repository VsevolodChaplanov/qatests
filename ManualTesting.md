# Ручное тестирование
## Проверка GET'ов query параметров


### GET запрос для параметра _q_
#### Запрос без указания значения параметра
Необрабатывается запрос с пустым значением параметра q
* Ввожу
``` sh
GET https://regions-test.2gis.com/1.0/regions?q
```
* Ответ сервера:
``` 
500 Internal Server Error

Server got itself in trouble
```
* Пустой запрос не обрабатывается сервером
* Также не обрабатывается запрос:

``` sh
GET https://regions-test.2gis.com/1.0/regions?q=
```
* Ответ сервера:
``` 
500 Internal Server Error

Server got itself in trouble
```
#### Запросы с указанием значения параметра

### GET запрос для параметра _country_code_
#### Запросы с указанием значения параметра
В ответе сервера для парамтреов "kg" и "kz", присутсвуются города и Кыргызстана и Казахстана, для обоих случаев
* Ввожу
``` sh
GET https://regions-test.2gis.com/1.0/regions?country_code=ua
```
* Ввожу
``` sh
GET https://regions-test.2gis.com/1.0/regions?country_code=kz
```
* Ожидаем в ответе города Казахстана
* Ответ сервера:
``` json
{
    "total": 22,
    "items": [
        {
            "id": 196,
            "name": "Актау",
            "code": "aktau",
            "country": {
                "name": "Казахстан",
                "code": "kz"
            }
        },
        {
            "id": 167,
            "name": "Актобе",
            "code": "aktobe",
            "country": {
                "name": "Казахстан",
                "code": "kz"
            }
        },
        {
            "id": 67,
            "name": "Алматы",
            "code": "almaty",
            "country": {
                "name": "Казахстан",
                "code": "kz"
            }
        },
        {
            "id": 112,
            "name": "Бишкек",
            "code": "bishkek",
            "country": {
                "name": "Кыргызстан",
                "code": "kg"
            }
        },
        {
            "id": 171,
            "name": "Ош",
            "code": "osh",
            "country": {
                "name": "Кыргызстан",
                "code": "kg"
            }
        },
        {
            "id": 91,
            "name": "Усть-Каменогорск",
            "code": "ustkam",
            "country": {
                "name": "Казахстан",
                "code": "kz"
            }
        }
    ]
}
```
* Смешанный ответ содержащий не только города Казахстана - ```ошибка```

* Ввожу
``` sh
GET https://regions-test.2gis.com/1.0/regions?country_code=kg
```
* Ожидаем в ответе города Кыргызстана
* Ответ сервера:
``` json
{
    "total": 22,
    "items": [
        {
            "id": 196,
            "name": "Актау",
            "code": "aktau",
            "country": {
                "name": "Казахстан",
                "code": "kz"
            }
        },
        {
            "id": 167,
            "name": "Актобе",
            "code": "aktobe",
            "country": {
                "name": "Казахстан",
                "code": "kz"
            }
        },
        {
            "id": 67,
            "name": "Алматы",
            "code": "almaty",
            "country": {
                "name": "Казахстан",
                "code": "kz"
            }
        },
        {
            "id": 112,
            "name": "Бишкек",
            "code": "bishkek",
            "country": {
                "name": "Кыргызстан",
                "code": "kg"
            }
        },
        {
            "id": 171,
            "name": "Ош",
            "code": "osh",
            "country": {
                "name": "Кыргызстан",
                "code": "kg"
            }
        },
        {
            "id": 91,
            "name": "Усть-Каменогорск",
            "code": "ustkam",
            "country": {
                "name": "Казахстан",
                "code": "kz"
            }
        }
    ]
}
```
* Смешанный ответ содержащий не только города Кыргызстана - ```ошибка```

### GET запрос для параметра _page_

Значение для параметра _page_size_ по умолчанию в документации -- 15, действительное значение по умолчанию -- 10

* Ввожу 
``` sh
GET https://regions-test.2gis.com/1.0/regions?page=1
```
* Ответ сервера:
``` json
{
    "total": 22,
    "items": [
        {
            "id": 196,
            "name": "Актау",
            "code": "aktau",
            "country": {
                "name": "Казахстан",
                "code": "kz"
            }
        },
        {
            "id": 167,
            "name": "Актобе",
            "code": "aktobe",
            "country": {
                "name": "Казахстан",
                "code": "kz"
            }
        },
        {
            "id": 67,
            "name": "Алматы",
            "code": "almaty",
            "country": {
                "name": "Казахстан",
                "code": "kz"
            }
        },
        {
            "id": 112,
            "name": "Бишкек",
            "code": "bishkek",
            "country": {
                "name": "Кыргызстан",
                "code": "kg"
            }
        },
        {
            "id": 114,
            "name": "Владикавказ",
            "code": "vladikavkaz",
            "country": {
                "name": "Россия",
                "code": "ru"
            }
        },
        {
            "id": 25,
            "name": "Владивосток",
            "code": "vladivostok",
            "country": {
                "name": "Россия",
                "code": "ru"
            }
        },
        {
            "id": 105,
            "name": "Днепр",
            "code": "dnepropetrovsk",
            "country": {
                "name": "Украина",
                "code": "ua"
            }
        },
        {
            "id": 7,
            "name": "Красноярск",
            "code": "krasnoyarsk",
            "country": {
                "name": "Россия",
                "code": "ru"
            }
        },
        {
            "id": 26,
            "name": "Магнитогорск",
            "code": "magnitogorsk",
            "country": {
                "name": "Россия",
                "code": "ru"
            }
        },
        {
            "id": 32,
            "name": "Москва",
            "code": "moscow",
            "country": {
                "name": "Россия",
                "code": "ru"
            }
        }
    ]
}
```
Без указания _page_size_ параметра, число выводимых регионов -- 10

Конечное значение текущей страницы совпадает с начальным значением следущией страницы
* Ввожу:
``` sh
GET https://regions-test.2gis.com/1.0/regions?page=1
```
* Ответ сервера:
``` json
{
    "total": 22,
    "items": [
        {
            "id": 196,
            "name": "Актау",
            "code": "aktau",
            "country": {
                "name": "Казахстан",
                "code": "kz"
            }
        },
		...
        {
            "id": 32,
            "name": "Москва",
            "code": "moscow",
            "country": {
                "name": "Россия",
                "code": "ru"
            }
        }
    ]
}
```
* Далее ввожу 
``` sh
GET https://regions-test.2gis.com/1.0/regions?page=2
```
* Ответ сервера:
``` json

    "total": 22,
    "items": [
        {
            "id": 32,
            "name": "Москва",
            "code": "moscow",
            "country": {
                "name": "Россия",
                "code": "ru"
            }
        },
		...
        {
            "id": 3,
            "name": "Томск",
            "code": "tomsk",
            "country": {
                "name": "Россия",
                "code": "ru"
            }
        }
    ]
}
```
* Далее ввожу:
``` sh
GET https://regions-test.2gis.com/1.0/regions?page=3
```
* Ответ сервера:
``` json
{
    "total": 22,
    "items": [
        {
            "id": 3,
            "name": "Томск",
            "code": "tomsk",
            "country": {
                "name": "Россия",
                "code": "ru"
            }
        },
		...
        {
            "id": 17,
            "name": "Уфа",
            "code": "ufa",
            "country": {
                "name": "Россия",
                "code": "ru"
            }
        }
    ]
}
```
Т.к. id совпадает, то это одинаковые данные, всего уникальных 21, вместо 22, подобное для каждого из параметров _page_size_ : {5,10,15}


В мануале указано, что параметр _page_ равен 1 или более, но при задании равно 0, возвращается ошибка 500
* Ввожу 
``` sh
GET https://regions-test.2gis.com/1.0/regions?page=0
```
* Ответ сервера
``` json
500 Internal Server Error

Server got itself in trouble
```
Для параметрв _page_size_ = 0 или любому другому числу не равному {5,10,15} ошибка обрабатывается