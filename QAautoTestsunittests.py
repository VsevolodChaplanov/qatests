import json
from urllib import request
import unittest
import requests

class TestAPI(unittest.TestCase):

	url = 'https://regions-test.2gis.com/1.0/regions'

	# ----------------- Required ----------------- #
	def test_total_number_of_regions(self):
		data = request.urlopen(
			self.url
			)
		body = json.loads(data.read().decode('utf-8'))
		self.assertEqual(body['total'], 22, msg = "Число регионов не 22")

	# ----------------- Query param q = рск ----------------- #
	def test_query_param_q(self):
		'''
		Used request.get( ... )
		Because request.urlopen( ... ) didn't work
			with russian letters
		'''
		data = requests.get(self.url + "?q=" + ("рск"))
		regions = json.loads(data.text)

		for region in regions['items']:
			city = region["name"]
			self.assertIn(
				'рск', city, 
				msg = "Среди регионов, есть такие которые не содержат \'рск\'"
			)

	@unittest.expectedFailure
	def test_query_param_q_empty(self):
		data = request.urlopen(
			self.url + '?q'
			)
		regions = json.loads(data.read().decode('utf-8'))['items']

	# ----------------- Query param country_code = "ru" ----------------- #
	def test_query_param_country_code_eq_ru(self):

		data = request.urlopen(
			self.url + '?country_code=ru'
			)
		regions = json.loads(data.read().decode('utf-8'))["items"]

		for item in regions:
			self.assertEqual(item["country"]["code"], "ru"
				, msg = "Присутсвуют города не из региона ru")

	# ----------------- Query param country_code = "ua" ----------------- #
	def test_query_param_country_code_eq_ua(self):

		data = request.urlopen(
			self.url + '?country_code=ua'
			)
		regions = json.loads(data.read().decode('utf-8'))["items"]

		for item in regions:
			self.assertEqual(item["country"]["code"], "ua"
				, msg = "Присутсвуют города не из региона ua")

	# ----------------- Query param country_code = "kz" ----------------- #
	def test_query_param_country_code_eq_kz(self):

		data = request.urlopen(
			self.url + '?country_code=kz'
			)
		regions = json.loads(data.read().decode('utf-8'))["items"]

		for item in regions:
			self.assertEqual(item["country"]["code"], "kz"
				, msg = "Присутсвуют города не из региона kz")

	# ----------------- Query param country_code = "kg" ----------------- #
	def test_query_param_country_code_eq_kg(self): 

		data = request.urlopen(
			self.url + '?country_code=kg'
			)
		regions = json.loads(data.read().decode('utf-8'))["items"]

		for item in regions:
			self.assertEqual(item["country"]["code"], "kg"
				, msg = "Присутсвуют города не из региона kg")
	
	# ----------------- Pages length  ----------------- #
	def test_query_param_page_len(self):

		## Подгружаю сразу 3 страницы, т.к. БД небольшая
		## Стоит подгружать по странице и сохранить последние и первые элементы
		data_first_page = request.urlopen(
			self.url + "?page=1"
			)
		regions_first_page = json.loads(data_first_page.read().decode('utf-8'))["items"]

		data_scnd_page = request.urlopen(
			self.url + "?page=2"
			)
		regions_scnd_page = json.loads(data_scnd_page.read().decode('utf-8'))["items"]

		data_third_page = request.urlopen(
			self.url + "?page=3"
			)
		regions_third_page = json.loads(data_third_page.read().decode('utf-8'))["items"]

		self.assertLessEqual(len(regions_first_page), 10, msg = "Число регионов на странице больше 10")
		self.assertLessEqual(len(regions_scnd_page), 10, msg = "Число регионов на странице больше 10")
		self.assertLessEqual(len(regions_third_page), 10, msg = "Число регионов на странице больше 10")

	# ----------------- Query param page = 1, 2, 3 ----------------- #
	def test_query_param_page(self):

		## Подгружаю сразу 3 страницы, т.к. БД небольшая
		## Стоит подгружать по странице и сохранить последние и первые элементы
		data_first_page = request.urlopen(
			self.url + "?page=1"
			)
		regions_first_page = json.loads(data_first_page.read().decode('utf-8'))["items"]

		data_scnd_page = request.urlopen(
			self.url + "?page=2"
			)
		regions_scnd_page = json.loads(data_scnd_page.read().decode('utf-8'))["items"]

		data_third_page = request.urlopen(
			self.url + "?page=3"
			)
		regions_third_page = json.loads(data_third_page.read().decode('utf-8'))["items"]

		self.assertNotEqual(
			regions_first_page[-1], regions_scnd_page[0],
			msg = "Совпадение регионов на страницах"
		)
		self.assertNotEqual(
			regions_scnd_page[-1], regions_third_page[0],
			msg = "Совпадение регионов на страницах"
		)

	# ----------------- Query param page_size = 5 ----------------- #
	def test_query_param_page_size_eq_5(self):

		data = request.urlopen(
			self.url + "?page=1&page_size=5"
			)
		regions = json.loads(data.read().decode('utf-8'))["items"]
		self.assertEqual(len(regions), 5, msg = 'Число регионов не равно 5')


	# ----------------- Query param page_size = 10 ----------------- #
	def test_query_param_page_size_eq_10(self):

		data = request.urlopen(
			self.url + "?page=1&page_size=10"
			)
		regions = json.loads(data.read().decode('utf-8'))["items"]

		self.assertEqual(len(regions), 10, msg = 'Число регионов не равно 10')


	# ----------------- Query param page_size = 15 ----------------- #
	def test_query_param_page_size_eq_15(self):

		data = request.urlopen(
			self.url + "?page=1&page_size=15"
			)
		regions = json.loads(data.read().decode('utf-8'))["items"]

		self.assertEqual(len(regions), 15, msg = 'Число регионов не равно 15')


if __name__ == 'main':
	unittest.main()